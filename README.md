## Project citas-react

citas-react

An application used to create cites of veterinary, built with React, JavaScript, and tailwind.

## Installation and Setup Instructions

Clone down this repository. You will need `node` and `npm` installed globally on your machine.

Installation:

`npm install`

To Run Test Suite:

`npm test`

To Run Dev:

`npm run dev`

To Start Server:

`npm start`

To Visit App:

`localhost:3000/`
