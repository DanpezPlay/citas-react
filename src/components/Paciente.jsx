function Paciente(){
    return (
        <div className="m-3 bg-white shadow-md rounded-lg py-10 px-5">
            <p className="font-bold mb-3 text-gray-700 uppercase">Mascota: <span className="font-normal normal-case">Zeus</span></p>
            <p className="font-bold mb-3 text-gray-700 uppercase">Propietario: <span className="font-normal normal-case">Kevin</span></p>
            <p className="font-bold mb-3 text-gray-700 uppercase">Email: <span className="font-normal normal-case">kevingonzalez552@gmail.com</span></p>
            <p className="font-bold mb-3 text-gray-700 uppercase">Fecha alta: <span className="font-normal normal-case">28/01/2023</span></p>
            <p className="font-bold mb-3 text-gray-700 uppercase">Sintomas: <span className="font-normal normal-case">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsum rem aliquam, ea totam, in sapiente, architecto temporibus debitis expedita voluptatibus consequatur placeat iste magnam ducimus aspernatur qui nesciunt corporis vitae.</span></p>
        </div>
    )
}

export default Paciente