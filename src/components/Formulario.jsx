function Formulario(){
    return (
        <div className="md:w-1/2 lg:w-2/5 mt-12">
            <h2 className="font-black text-3xl text-center">Seguimiento Pacientes</h2>
            <p className="text-lg mt-5 text-center mb-10">Añade pacientes y <span className="text-indigo-600 font-bold">administralos</span></p>

            <form action="" className="bg-white shadow-md rounded-lg py-10 px-5">
                <div className="mb-5">
                    <label htmlFor="nombreMascota" className="block text-gray-700 uppercase font-bold">Nombre mascota</label>
                    <input type="text" placeholder="Nombre de la mascota..." className="border-2 w-full p-2 mt-2 rounded-md" id="nombreMascota"/>
                </div>
                <div className="mb-5">
                    <label htmlFor="nombrePropietario" className="block text-gray-700 uppercase font-bold">Nombre propietario</label>
                    <input type="text" placeholder="Nombre del propietario..." className="border-2 w-full p-2 mt-2 rounded-md" id="nombrePropietario"/>
                </div>
                <div className="mb-5">
                    <label htmlFor="email" className="block text-gray-700 uppercase font-bold">Email</label>
                    <input type="email" placeholder="Email..." className="border-2 w-full p-2 mt-2 rounded-md" id="email"/>
                </div>
                <div className="mb-5">
                    <label htmlFor="fechaAlta" className="block text-gray-700 uppercase font-bold">Fecha alta</label>
                    <input type="date" className="border-2 w-full p-2 mt-2 rounded-md" id="fechaAlta"/>
                </div>
                <div className="mb-5">
                    <label htmlFor="sintomas" className="block text-gray-700 uppercase font-bold">Sintomas</label>
                    <textarea placeholder="Describa los sintomas de la mascota..." className="border-2 w-full p-2 mt-2 rounded-md" id="sintomas"/>
                </div>
                <div>
                    <input type="submit" className="bg-indigo-600 w-full p-3 text-white rounded-md font-bold hover:bg-indigo-700 cursor-pointer transition-all" value="Agregar Paciente" />
                </div>
            </form>
        </div>
    )
}

export default Formulario